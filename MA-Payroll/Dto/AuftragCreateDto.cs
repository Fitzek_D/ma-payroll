﻿using System;
using System.Collections.Generic;

namespace MA_Payroll.Dto
{
    public class AuftragCreateDto
    {
        public string Bezeichnung { get; set; }
        public int Dauer { get; set; }
        public int UserId { get; set; }
        public int Nummer { get; set; }
        public List<ArbeiterDto> Arbeiter { get; set; }
    }
}
