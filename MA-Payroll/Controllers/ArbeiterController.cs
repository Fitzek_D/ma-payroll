﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MA_Payroll.Db;
using MA_Payroll.Dto;
using MA_Payroll.DbModell;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Text.Json;
using System.IO;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MA_Payroll.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ArbeiterController : Controller
    {
        [HttpGet]
        [ActionName("get-arbeiters")]
        public ActionResult<List<ArbeiterDto>> GetArbeiter([FromServices] Database db, int id)
        {
            var arbeiterliste = new List<ArbeiterDto>();

            if (id != 0)
            {
                var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == id);

                var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

                var zeitarbeiter = db.Zeitarbeiter.ToList();


                if (kunde.Zeitarbeiter != null)
                {
                var arbeiter = kunde.Zeitarbeiter.ToList();


                    foreach (var arbeit in arbeiter) {

                        var lohn = arbeit.Gehalt * arbeit.StundenImMonat;

                        var arbeiterDto = new ArbeiterDto
                        {
                            Name = arbeit.Name,
                            Id = arbeit.Id,
                            Stunden = arbeit.StundenImMonat,
                            Lohn = lohn,
                        };

                        arbeiterliste.Add(arbeiterDto);
                    }

                    return Ok(arbeiterliste);
                }
            }

            return Ok(arbeiterliste);
        }

        [HttpGet]
        [ActionName("get-arbeiter-by-id")]
        public ActionResult<ArbeiterDto> GetArbeiterById([FromServices] Database db, int id)
        {
            if (id != 0)
            {
                var zeitarbeiter = db.Zeitarbeiter.SingleOrDefault(a => a.Id == id);

                var lohn = zeitarbeiter.Gehalt * zeitarbeiter.StundenImMonat;

                var arbeiterDto = new ArbeiterDto
                {
                    Name = zeitarbeiter.Name,
                    Id = zeitarbeiter.Id,
                    Stunden = zeitarbeiter.StundenImMonat,
                    Lohn = lohn
                };

                return Ok(arbeiterDto);
            }

            return Ok();
        }

        [HttpPost]
        [ActionName("add-arbeiter")]
        public ActionResult AddArbeiter([FromBody] ArbeiterCreateDto newArbeiter, [FromServices] Database db)
        {
            var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == newArbeiter.UserId);

            var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

            var arbeiter = new Zeitarbeiter
            {
                Name = newArbeiter.Name,
                Email = newArbeiter.Email,
                Handynummer = newArbeiter.Tel,
                Gehalt = newArbeiter.Gehalt,
                Kunde = kunde,
            };

            db.Zeitarbeiter.Add(arbeiter);
            db.SaveChanges();

            return Ok();
        }

        [HttpPost]
        [ActionName("add-stunden")]
        public ActionResult AddArbeiter([FromBody] ArbeitszeitDto arbeitszeit, [FromServices] Database db)
        {
            var arbeiter = db.Zeitarbeiter.SingleOrDefault(a => a.Id == arbeitszeit.ArbeiterId);

            arbeiter.StundenImMonat += arbeitszeit.StundenToAdd;

            db.Zeitarbeiter.Update(arbeiter);
            db.SaveChanges();

            return Ok();
        }

        [HttpPost]
        [ActionName("add-abrechnung")]
        public ActionResult AddAbrechnung([FromServices] Database db, int id)
        {
            var zeitarbeiter = db.Zeitarbeiter.SingleOrDefault(a => a.Id == id);

            var lohn = zeitarbeiter.Gehalt * zeitarbeiter.StundenImMonat;

            var lohnabrechnung = new Lohn
            {
                Betrag = lohn,
                Zeitarbeiter = zeitarbeiter
            };

            db.Loehne.Add(lohnabrechnung);
            db.SaveChanges();

            var lohnEmail = new LohnDto
            {
                Name = zeitarbeiter.Name,
                Email = zeitarbeiter.Email,
                Betrag = lohn,
            };

            var json = JsonSerializer.Serialize(lohnEmail);
            
            var url = "https://ma-email-df.azurewebsites.net/api/email/send-email";

            var request = WebRequest.Create(url);
            request.Method = "POST";

            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine(result);
            }

            var lohnSms = new SmsDto
            {
                Name = zeitarbeiter.Name,
                Handynummer = zeitarbeiter.Handynummer
            };

            var smsJson = JsonSerializer.Serialize(lohnSms);

            var smsUrl = "https://ma-sms-df.azurewebsites.net/api/sms/send-lohnabrechnung";

            var smsRequest = WebRequest.Create(smsUrl);
            smsRequest.Method = "POST";

            smsRequest.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(smsRequest.GetRequestStream()))
            {
                streamWriter.Write(smsJson);
            }

            var httpSmsResponse = (HttpWebResponse)smsRequest.GetResponse();
            using (var streamReader = new StreamReader(httpSmsResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine(result);
            }

            zeitarbeiter.StundenImMonat = 0;
            db.Zeitarbeiter.Update(zeitarbeiter);
            db.SaveChanges();

            return Ok();
        }

    }
}
