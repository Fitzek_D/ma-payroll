import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";


@Injectable({
    providedIn: 'root'
})

export class AccountIdService {
    
    userId: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    constructor() {}

    setId(id: number){
        this.userId.next(id);
    }
}
