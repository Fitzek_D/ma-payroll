import { ArbeiterDto } from "./ArbeiterDto";

export interface AuftragCreateDto {
    nummer: number,
    bezeichnung: string,
    dauer: number,
    arbeiter: ArbeiterDto[],
    userId: number
}