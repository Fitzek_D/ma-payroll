import { ProduktDto } from "./ProduktDto";

export interface AccountDto {
    isValid: boolean;
    benutzername: string;
    rolle: string;
    id: number;
}